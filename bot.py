# Work with Python 3.6
from builtins import map

import discord
import logging
import TokenFile
import random
from random import randint
import pickle
import math
from time import sleep
from time import clock
from System import Profile
from System import  BLIND_ID
from System import get_role
from Equipment import count_through
from Equipment import Restraint
import Equipment
from discord.utils import get
import re


def count_urls( string ):
    #counts how many urls are in the string
    return len( re.findall('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', string ) )

def roll( maximum, count=1 ):
    return_me = 0
    for i in range(count):
        return_me += random.randint( 1, maximum )
    return return_me

def update( player ):
    #player is a discord member object
    new = Profile( player )
    try:
        old = players[ player.id ]
    except KeyError:
        old = Profile( player )

    for var, val in new.__dict__.items():
        try:
            new.__dict__[ var ] = old.__dict__[ var ]
        except KeyError:
            pass

    players[ player.id ] = new

def members_with_role( server, role_id=None, role=None):
    if role_id is None and role is None:
        raise ImportError
    else:
        return_me = []
        if role is None:
            role = get_role( server, role_id )
        for member in server.members:
            if role in member.roles:
                return_me.append( member )
        return return_me

def get_channel( player, message ):
    if message.server is None:
        for role in player.member.roles:
            if role.id==BLIND_ID:
                return player.blinded_in
        print( message.author.display_name + ' DMed the bot ' + message.content )
        return None
    else:
        if player.gag.spell.name == 'Nothing':
            return None
        else:
            return message.channel

my_server_id = '704043066281033850'
BOSS = '479536998072188928'

BIND_ROLES = {
    #channel id             role id
    '704048256316866601' : '704292290977595493', #rp1
    '704048300667699671' : '704305215653937202', #rp2
    '704048343130570812' : '704305217822261258', #rp3
}

client = discord.Client()
logging.basicConfig(level=logging.INFO)
CHARACTER_REACTION = '⬆'
commands = [ '`list <arg>:` lists different players, servers, spells, etc.',
             '`allow @player:` allows the player to use the bot on you',
             '`roll:` I roll dice for you.',
             '`save:` you save the current state of the bot.',
             '`load:` you load to the last save of the bot.',
             '`escape:` you try to escape your bonds.',
             '`future:` lists the things I want to code next.',
             "---the user must !allow you in order to perform the following---"
             '`release @user:` you release the player that you mention from blindfolds and bindings.',
             '`gag @user:` the mentioned user is gagged',
             '`ungag @user:` the mentioned user is ungagged.',
             '`!blind @user:` the mentioned user is blinded.',
             '`!unblind @user:` removes the blindfold from the mentioned user.',
             '`bind @user:` binds the user to the channel.',
             '`leash @user:` pulls the user to the new channel',
             '`spank @user:` '
             ]
goddess_commands = [
    'release',
    'gag',
    'ungag',
    'blind',
    'unblind',
    'bind',
    'leash',
    'spank'
]
eval_functions = {
                    'randint' :random.randint,
                    'min'   :min,
                    'max'   :max,
                    'round' :round,
                    'ceil'  :math.ceil,
                    'floor' :math.floor,
                    'roll'  :roll,
                    'd'     :roll
                }
players = {}

@client.event
async def on_member_join( member ):
    if not member.id in players.keys():
        players[ member.id ] = Profile( member )
    update( member )
    print( member.display_name + ' has joined' )

@client.event
async def on_member_update( old, new):
    if old.server.id==my_server_id and (old.display_name!=new.display_name or old.avatar_url!=new.avatar_url or\
            old.roles!=new.roles):
        print( old.display_name + ' updated to '+new.display_name )
        update( new )


@client.event
async def on_reaction_add( reaction, player ):
    print(reaction.emoji)
    if reaction.me and reaction==CHARACTER_REACTION:
        pass
        #Character(reaction.message)
        #await client.send_message("character created")

'''
@client.event
async def on_error( event, *args, **kwargs ):
    if event=='on_message':
        message = args[ 0 ]
        await client.send_message( message.channel, message.content+' has broken the bot <@!'+BOSS+'>' )
        await client.send_message( players[BOSS].member, message.content+' has broken the bot' )
    else:
        print( 'event' )
        for error in event:
            print( error )
        print( 'args' )
        for error in args:
            print( error )
        print( 'kwargs' )
        for error in kwargs:
            print( error )
'''

@client.event
async def on_message( message ):
    global players

    def end():
        if '(' in message.content or '#' in message.content or '.' in message.content:
            print( message.content )
            if message.author.id==BOSS:  #my ID
                my_players = [ ]
                for player in message.mentions:
                    my_players.append( players[ player.id ] )
                try:
                    out = eval( message.content[ 1: ] )
                    try:
                        index = int( message.content.index( '#' ) )
                        return message.content[ index: ]+'\n```'+str( out )+'```'
                    except ValueError:
                        return '```'+str( out )+'```'
                except SyntaxError:
                    exec( message.content[ 1: ] )
            else:
                out = eval( message.content[ 1: ], { '__builtins__':None }, eval_functions )
                try:
                    index = int( message.content.index( '#' ) )
                    return message.content[ index: ]+'\n```'+str( out )+'```'
                except ValueError:
                    return '```'+str( out )+'```'
        else:
            return 'Use !help to get help. You need it'

    if message.author == client.user:   #if I am looking at my own message
        '''send my messages to the blinded people but watch out for me replying to that function recursively
        if message.server is None:
            print( message.channel.recipient.id + '=' + BOSS )

        else:
            print( message.server.name )
            for member in members_with_role( message.server, BLIND_ID ):
                if players[ member.id ].blinded_in.id==message.channel.id:
                    await client.send_message( member, '\'' + message.content )'''
        return

    outputs = [] #this is where I store messages. At the end I send all of them together

    for pic in message.attachments:
        message.content += ' '+pic[ 'url' ]

    this_player = players[ message.author.id ]
    if len( message.mentions ) > 0:
        target = players[ message.mentions[ 0 ].id ]
    else:
        target = None

    content = ''
    embed = discord.Embed()
    channel = None

    if not message.content.startswith( '!' ) and this_player.gag.tightness > 0 and not message.server is None:
        await client.delete_message( message )
        print( message.author.name + ': ' + message.content )
        embed.set_thumbnail( url=this_player.member.avatar_url )
        embed.description = this_player.gag.gag_the_message( message.content )
        embed.colour = discord.Colour( 15277667 )
        channel = message.channel

    if channel is not None:
        try:
            await client.send_message( channel, embed=embed, content=content )
            for member in members_with_role( this_player.member.server, role_id=BLIND_ID):
                if players[ member.id ].blinded_in.id==channel.id:
                    await client.send_message( member, embed=embed, content=content )
        except discord.DiscordException:
            try:
                if len( content ) > 0 and not content == len( content ) * ' ':
                    await client.send_message( channel, content=content )
                    for member in members_with_role( this_player.member.server, role_id=BLIND_ID):
                        if players[ member.id ].blinded_in.id==channel.id:
                            await client.send_message( member, content=content )
                else:
                    await client.send_message( channel, embed=embed )
                    for member in members_with_role( this_player.member.server, role_id=BLIND_ID):
                        if players[ member.id ].blinded_in.id==channel.id:
                            await client.send_message( member, embed=embed )
            except discord.DiscordException:
                await client.send_message( channel, this_player.gag.gag_the_message( message.content ) )
                for member in members_with_role( this_player.member.server, role_id=BLIND_ID ):
                    if players[ member.id ].blinded_in.id==channel.id:
                        await client.send_message( member, this_player.gag.gag_the_message( message.content ) )

    else:
        pass
        #if message.server is not None:
        #    react = True
        #    for member in members_with_role( message.server, role_id=BLIND_ID):
        #        if players[ member.id ].blinded_in.id==message.channel.id:
        #            await client.send_message( member, content=message.content )
        #            if react:
        #                react = False
        #                await client.add_reaction( message, CHARACTER_REACTION )


    if not message.content.startswith( '!' ):
        return

    command = message.content.lower()[1:].split()[0]
    arg = message.content.lower().split()[1:]
    while len( arg ) < 2:
        arg.append( '' )    #MIGHT NOT WORK!

    if command=='help':
        for com in commands:
            outputs.append( com )

    elif command=='save':
        if arg!=[] and arg[ 0 ]=='help':
            await client.send_message( message.channel, 'Saves the current state of the bot.' )
        else:
            pickle.dump( players, open( 'save.p', 'wb' ) )
            await client.send_message( message.channel, 'data saved' )

    elif command=='load':
        if arg!=[] and arg[ 0 ]=='help':
            await client.send_message( message.channel, 'Loads the saved state of the bot to the current state.' )
        else:
            players = pickle.load( open( 'save.p', 'rb' ) )
            for each in client.servers:
                if each.name=='Bondage Workshop':
                    for player in each.members:
                        if not player.id in players.keys():
                            players[ player.id ] = Profile( player, temp_house )
                        player.name = player.nick if player.nick is not None else player.name
            await client.send_message( message.channel, 'data loaded, but changes are not saved until you use !save or '
                                        'do a command that saves (ie. !escape)' )

    elif command=='roll' or command == 'r':
        if arg==[] or ( arg[ 0 ]=='' and arg[ 1 ]=='' ):
            await client.send_message( message.channel, "```"+str(random.randint( 1, 20 ) )+"/20```" )
        elif arg[ 0 ]=='help':
            await client.send_message( message.channel, 'roll( <maximum>, <number> ) or roll <maximum> or talk to my coder '+\
                                       players[ BOSS ].member.mention )
        else:
            try:
                if int( arg[ 0 ] )<=0:
                    raise ValueError
                await client.send_message( message.channel,
                    "```"+str(random.randint( 1, int( arg[ 0 ] ) ) )+"/"+str( arg[ 0 ] )+"```" )
            except ValueError:
                await client.send_message( message.channel, "use positive whole numbers only" )

    elif 'future'==command:

        if arg==[ ] or arg[ 0 ]=='help':
            await client.send_message( message.channel, 'I tell you want my developer wants to do with me next.' )
        else:
            await client.send_message( message.channel, '\n'.join( [ str( index+1 )+'. '+value for index, value in enumerate( [
            'notify captors of escapees',
            'legs',
            'vibrators',
            'controller collar',
            'nipple leash',
            'jail',
            'brand/name tag',
            'stats, like how long your rp posts are, how many'
            ' posts you\'ve made, how fast your reply time is, what your favorite words are in rp that aren\'t '
            'used in regular conversation much',
            '!sins',
            'blinded people can walk around',
            '`!submit` makes a message that players can :thumbsup: to allow the player. Using `!submit @player` causes the '
            'mentioned player to kneel when they hit the :thumbsup:, but other players are ignored by the bot.',
        ] ) ] ) )

    elif command.startswith( 'allow' ):
        if arg== [] or arg[ 0 ] == 'help':
            await client.send_message( message.channel, 'usage `!allow @user`. Allows the mentioned user to use the bot on you.' )
        elif len( message.mentions ) == 0:
            await client.send_message( message.channel, 'You must mention a user to allow them to user the bot on you.' )
        elif this_player.member.id == message.mentions[ 0 ].id:
            await client.send_message( message.channel, 'You cannot allow yourself control' )
        else:
            this_player.doms.append( target.member.id )
            pickle.dump( players, open( 'save.p', 'wb' ) )
            await client.send_message( message.channel, 'You have given them control.' )

    elif command.startswith( 'unallow' ):
        if arg== [] or arg[ 0 ] == 'help':
            await client.send_message( message.channel, 'usage `!unallow @user`. Does not allow the user to use the bot on you.' )
        elif len( message.mentions ) == 0:
            await client.send_message( message.channel, 'You must mention a user to stop them from using the bot on you.' )
        elif this_player.member.id == message.mentions[ 0 ].id:
            await client.send_message( message.channel, 'You cannot unallow yourself.' )
        else:
            try:
                this_player.doms.remove( target.member.id )
                pickle.dump( players, open( 'save.p', 'wb' ) )
                await client.send_message( message.channel, target.member.display_name + ' cannot use the bot on you.' )
            except ValueError:
                await client.send_message( message.channel, target.member.display_name + ' cannot use the bot on you.' )

    elif command.startswith( 'safeword' ):
        this_player.gag.tightness = 0
        this_player.channel = None
        this_player.blind = False
        this_player.enthrall = None
        this_player.chastity = False
        this_player.doms = []
        pickle.dump( players, open( 'save.p', 'wb' ) )
        await client.send_message( message.channel, this_player.member.display_name + ' released from all doms and conditions.' )

    elif 'escape'==command:
        if arg!=[] and arg[ 0 ]=='help':
            await client.send_message( message.channel, 'You try and escape from the locks that bind you. It will '
                    'fail if your roll is too low and add another lock if you critically fail.'
                    'There is a cool down that depends on the equipment.' )
        else:
            if this_player.restraint.locks == 0:
                await client.send_message( message.channel, 'You are already free' )
            else:
                await client.send_message( message.channel, this_player.restraint.struggle( this_player.luck ) )
                if this_player.restraint.locks == 0:
                    for role in this_player.member.roles:
                        if role.id in BIND_ROLES.values():
                            await client.remove_roles( this_player.member, role )

            players[ this_player.member.id ].restraint = this_player.restraint
            pickle.dump( players, open( 'save.p', 'wb' ) )




    elif arg != [] and arg[ 0 ] != 'help' and ( target is None or this_player.member.id not in target.doms ) and command in goddess_commands:
        await client.send_message( message.channel, 'They must !allow you to give you power over them.' )

    elif arg != [] and arg[ 0 ] != 'help' and ( target is None or this_player.member.id not in target.doms ) and command not in goddess_commands:
        await client.send_message( message.channel, end() )

    #------------------dom moves--------------------# ALL MUST MENTION SOMEONE
    #elif command.startswith( 'blind' ):
    #    if arg == [] or arg[0]=='help':
    #        await client.send_message( message.channel, 'usage `!blind @player`. A blindfold appears aroound the '
    #                                            'mentioned player\'s eyes. This is impossible to remove if bound but '
    #                                            'does not require the player to submit.' )
    #    else:
    #        await client.add_roles( member, get_role( message.server, BLIND_ID ) )
    #        target.channel = message.channel
    #        await client.send_message( message.channel, target.member.display_name+' is blinded.' )
    #        await client.send_message( member, 'someone has blinded you in '+message.channel.name )
    #        pickle.dump( players, open( 'save.p', 'wb' ) )
    #elif command.startswith( 'unblind' ):
    #    if arg == [] or arg[0]=='help':
    #        await client.send_message( message.channel, '`!unblind @user`. The blindfold disappears  from around the '
    #                                        'mentioned user\'s eyes. This command will not work if you are bound.' )
    #    elif len( message.mentions )==0:
    #        await client.send_message( message.channel, 'you need to mention a player.' )
    #    else:
    #        for member in message.mentions:
    #            await client.remove_roles( member, get_role( message.server, BLIND_ID ) )
    #            await client.send_message( message.channel, member.display_name+' is no longer blindfolded.' )

    elif command == 'spank':
        await client.send_message( message.channel, 'SMACK' )

    elif command == 'leash':
        if arg == [] or arg[ 0 ] == 'help':
            await client.send_message( message.channel, 'User `leash @user` to pull the user to the current channel and lock them in that channel.' )
        else:
            for role in target.member.roles:
                if role.id in BIND_ROLES.values():
                    await client.remove_roles( target.member, role )
            target.channel = message.channel
            await client.add_roles( target.member, get_role( message.server, BIND_ROLES[ message.channel.id ] ) )
            pickle.dump( players, open( 'save.p', 'wb' ) )
            await client.send_message( message.channel, target.member.display_name + ' bound in ' + target.channel.name )


    elif command=='bind':
        if arg==[] or arg[ 0 ]=='help':
            await client.send_message( message.channel, 'Use `bind @user` to bind a user that has allowed you to use the bot on them. ' +
                                       'A bound player cannot message other channels.' )
        else:
            for role in target.member.roles:
                if role.id in BIND_ROLES.values():
                    await client.remove_roles( target.member, role )
            if len( message.channel_mentions ) == 0:
                target.channel = message.channel
            else:
                target.channel = message.channel_mentions[ 0 ]
            await client.add_roles( target.member, get_role( message.server, BIND_ROLES[ target.channel.id ] ) )

            target.restraint.locks = 0
            for num in arg:
                try:
                    target.restraint = Restraint( struggle_length = 2, locks= int( num ), escape_dc= 10 )
                except ValueError:
                    pass
            if target.restraint.locks == 0:
                target.restraint = Restraint( struggle_length=2, locks=5, escape_dc=10 )
                await client.send_message( message.channel, target.member.display_name + ' bound in ' + target.channel.name )
            else:
                await client.send_message( message.channel, '{0} bound in {1} with {2} locks'.format( target.member.display_name, target.channel.name, target.restraint.locks ) )
            players[ target.member.id ].restraint = target.restraint
            pickle.dump( players, open( 'save.p', 'wb' ) )

    elif command=='gag':
        if arg==[] or arg[ 0 ]=='help':
            await client.send_message( message.channel, '`gag @user` the user is gagged. The people the user has allowed ' +
                                       'can use `ungag @user` to ungag the user.\n' +
                                       'Use `gag @user 100` for the tightest gag and `gag @user 10` for a weak gag (or any number in between)' )
        else:
            target.gag.tightness = 0
            for num in arg:
                try:
                    target.gag.tightness = int( num )
                except ValueError:
                    pass
            if target.gag.tightness == 0:
                await client.send_message( message.channel, 'User gagged.' )
                target.gag.tightness = 50
            else:
                await client.send_message( message.channel, 'User gagged with {0}.'.format( target.gag.tightness ) )
            pickle.dump( players, open( 'save.p', 'wb' ) )

    elif command=='ungag':
        if arg==[] or arg[ 0 ]=='help':
            await client.send_message( message.channel, '`ungag @user` the user is ungagged.' )
        else:
            target.gag.tightness = 0
            pickle.dump( players, open( 'save.p', 'wb' ) )
            await client.send_message( message.channel, target.member.display_name + ' ungagged.' )

    #elif command=='submit':
    #    if arg[ 0 ]=='help':
    #        await client.send_message( message.channel, '`!submit`: makes the next person that reacts :thumbsup: kneel. '
    #                                   '`!submit @player` makes the mentioned @player kneel if they react :thumbsup: and '
    #                                   'alerts you if they choose to resist.' )
    #    else:
    #        #if len( message.mentions )==0:
    #        msg = await client.send_message( message.channel, 'React :thumbsup: to kneel and react :thumbsdown: to resist.' )
    #        #else:
    #        #for member in message.mentions
    #        await client.add_reaction( message=msg, emoji='👍' )
    #        await client.add_reaction( message=msg, emoji='👎' )
    #        while True:
    #            res = await client.wait_for_reaction( [ '👍', '👎' ], message=msg )
    #            if res.user.id!=client.user.id and ( len( message.mentions )==0 or res.user in message.mentions):
    #                await client.delete_message( msg )
    #                break
    #        if res.reaction.emoji== '👎':
    #            if this_player.visible:
    #                await client.send_message( message.channel, this_player.member.mention+', '+res.user.mention+' did not submit.' )
    #            else:
    #                await client.send_message( message.channel, res.user.mention+' did not submit.' )
    #        elif res.reaction.emoji=='👍':
    #            if this_player.visible:
    #                await client.send_message( message.channel, this_player.member.mention+', '+res.user.mention+' submitted to you.' )
    #            else:
    #                await client.send_message( message.channel, res.user.mention+' submitted to you.' )
    #            players[ res.user.id ].helplessness = 21

    elif command=='release':
        if arg==[] or arg[ 0 ]=='help':
            await client.send_message( message.channel,
                                       '`release @user`. You remove the bindings and blindfold on the user.' )
        else:
            for role in target.member.roles:
                if role.id in BIND_ROLES.values():
                    await client.remove_roles( target.member, role )
            target.channel = None
            target.blind = False
            target.enthrall = None
            target.chastity = False
            pickle.dump( players, open( 'save.p', 'wb' ) )
            await client.send_message( message.channel, target.member.display_name + ' released.' )

    else:
        await client.send_message( message.channel, end() )

    output_me = '\n'.join( outputs )
    while len( output_me )>0: #output output_me 'safely'
        await client.send_message( message.channel, output_me[ :1999 ] )
        output_me = output_me[ 1999: ]

@client.event
async def on_ready():
    global players
    print( 'Logged in as' )
    print( client.user.name )
    try:
        players = pickle.load( open( 'save.p', 'rb' ) )
    except FileNotFoundError:
        players = {}

    mass_welcome = ''
    for each in client.servers:
        if each.id==my_server_id:
            #update all players
            for player in each.members:
                if not player.id in players.keys():
                    players[ player.id ] = Profile( player )
                    mass_welcome += player.name + '\n'
                update( player )
            pickle.dump( players, open( 'save.p', 'wb' ) )

    print( '------' )
    print( mass_welcome )
    pass

client.run(TokenFile.TOKEN)