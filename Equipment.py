from collections import defaultdict
from random import randint
import time

def count_through( arr, number ):
    if number<=0:
        return 0
    for index, this_index in enumerate( arr ):
        number-=this_index
        if number<=0:
            return index
    return len( arr ) + 1

class Restraint:
    def __init__( self, struggle_length = 0, locks = 0, escape_dc = 0 ):
        self.struggle_length = struggle_length
        self.struggle_clock = time.clock() - struggle_length
        self.locks = locks
        self.escape_dc = escape_dc

    def struggle( self, luck ):
        if self.struggle_length-(time.clock()-self.struggle_clock)>self.struggle_length:
            self.struggle_clock = time.clock()-self.struggle_length
        if time.clock()-self.struggle_clock>self.struggle_length:
            index = count_through( luck, randint( 1, sum( luck ) ) ) + 1
            self.struggle_clock = time.clock()
            if index>=self.escape_dc:
                return_me = ''
                if index == 20:
                    return_me = 'You are lucky enough to break 3 locks!\n'
                    self.locks -= 3
                else:
                    self.locks -= 1
                if self.locks<=0:
                    self.locks = 0
                    return return_me + str( index )+': You have broken free!'
                else:
                    return return_me + str( index )+': You have '+\
                           str( self.locks )+' locks to break out of before you are free.'
            elif index == 1:
                self.locks += 3
                return 'Your struggling only makes your binds tighter. There are now ' + str( self.locks ) + ' locks left.'
            else:
                return 'You try valiantly to escape, but your rolls were not high enough to break a lock ({0}/{1}).'.format( index,
                                                                                                    self.escape_dc )
        else:
            return 'You need to wait '+str( self.struggle_length-(time.clock()-self.struggle_clock) )+ \
                   ' more seconds before trying to escape again'

    def as_message( self ):
        return_me = 'locks left: '+str( self.restraint.locks )+'\n'
        return_me += 'difficulty check: '+str( self.restraint.escape_dc )+'\n'
        return_me += 'time between tries: '+str( self.restraint.struggle_length )+'\n'
        return return_me


class Gag:
    def __init__( self, parent_profile, tightness = 0 ):
        self.profile = parent_profile
        self.tightness = tightness
        self.name = 'empty'

    def gag_the_message( self, message_content ):
        gagspeak = ''
        italicized = False
        for index in range(0, len( message_content ) ):
            char = message_content[ index ]
            if ( char=='*' or char=='_' ) \
                    and not ( len( message_content ) > index+1 and message_content[index+1] == '*' and char=='*' ):
                italicized = not italicized

            gagspeak += ball_gag[ char ] if randint( 1, 100 )<self.tightness and ball_gag[ char ]!='None' and not italicized else char
        return '`'+self.profile.name+':`\n'+gagspeak

ball_gag = defaultdict( lambda: 'None', {
    'a':'h',
    'o':'h',
    'u':'h',
    'c':'g',
    'k':'g',
    'e':'m',
    'i':'hm',
    's':'f',
    'y':'n'
} )