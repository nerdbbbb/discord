from Equipment import count_through
from Equipment import Gag
from time import clock
from random import randint
from Equipment import Restraint

def get_role( server, role_id ):
    for role in server.roles:
        if role.id == role_id:
            return role
BLIND_ID = '567416117233057824'

def complete_update( players ):
    for key, each in players.items():
        players[ key ] = Profile( each.member )

class Profile:
    def __init__( self, member,  gag = None, doms = [], channel = None, blind = False,  enthrall = None, chastity = False, restraint = None, luck = None ):
        self.member = member
        self.name = member.display_name
        if gag is None:
            self.gag = Gag( parent_profile=self )
        else:
            self.gag = gag
        self.doms = doms
        self.channel = channel
        self.blind = blind
        self.enthrall = enthrall
        self.chastity = chastity
        if restraint is None:
            self.restraint = Restraint()
        else:
            self.restraint = restraint
        if luck is None:
            self.luck = [ 5 for _ in range( 0, 20 ) ]
            self.luck[ 0 ] = 10
        else:
            self.luck = luck

    def as_message( self ):
        return str( self.__dict__ )